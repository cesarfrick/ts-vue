import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { ErrorState, State, Student } from './types'

Vue.use(Vuex)

const errorSystem = {
  state: {
    show: false,
    message: '',
  },
  mutations: {
    showError(state: ErrorState, message: string):void {
      state.show = true
      state.message = message
    },
  },
}

const state: State = {
  students: [],
  isLoaded: false,
}

const students = (state: State) => state.students.map((student: Student) => ({
  ...student,
  fullName: `${student.firstName} ${student.lastName}`,
}))

export const store = new Vuex.Store({
  state,
  getters: {
    students:string[] (state:State) => state.students.map((student:string[] )=> ({
      ...student,
      fullName: `${student.firstName} ${student.lastName}`,
    })),
    findStudent: state => id => state.students.find(student => student.id === Number(id)),
  },
  mutations: {
    setStudents(state, students) {
      state.students = students
    },
    addStudent(state, student) {
      state.students.push(student)
    },
    editStudent(state, student) {
      const index = state.students.findIndex(st => st.id === student.id)
      Vue.set(state.students, index, student)
    },
    isLoaded(state, isLoaded) {
      state.isLoaded = isLoaded
      Vue.set(state, 'isLoaded', isLoaded)
    },
  },
  actions: {
    async getStudents(context) {
      context.commit('isLoaded', false)

      try {
        const students = (await axios.get('http://localhost:3000/students')).data
        context.commit('setStudents', students)
        context.commit('isLoaded', true)
      } catch (error) {
        context.commit('showError', error)
      }
    },
    async createStudent(context, {
      firstName,
      lastName
    }) {
      const student = (await axios.post("http://localhost:3000/students", {
        firstName,
        lastName
      })).data

      context.commit('addStudent', student)
    },
    async editStudent(context, {
      id,
      firstName,
      lastName
    }): Promise < void > {
      const student: Student = (await axios.put(`http://localhost:3000/students/${id}`, {
        firstName,
        lastName
      })).data

      context.commit('editStudent', student)
    },
  },
  modules: {
    error: errorSystem,
  }
})
