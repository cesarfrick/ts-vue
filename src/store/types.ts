export type ErrorState = {
  show: boolean
  message: string
}
export type Student = {
  firstName: string
  lastName: string
}

export type State = {
  students: Student[]
  isLoaded: boolean
}
